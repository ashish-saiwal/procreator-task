$(document).on("click", "#search", function() {
  if ($(this).hasClass("open")) {
    $(this).removeClass('open')
    $(".fade-away").css({
      transform: "translateX(0%)"
    });
    $(".fade-in").css({
        transform: "translateX(100%)"
      });
      $('#search-input').css('opacity','0')
  } else {

    $(this).addClass('open')
    $(".fade-away").css({
        transform: "translateX(-100%)"
      });
      $(".fade-in").css({
        transform: "translateX(0%)"
      });
      $('#search-input').css('opacity','1')
      
  }
});
